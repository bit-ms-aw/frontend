package com.muhardin.endy.training.microservices.frontend.controller;

import com.muhardin.endy.training.microservices.frontend.service.PembayaranClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller @Slf4j
public class BackendInfoController {

    @Autowired private PembayaranClientService pembayaranClientService;

    @GetMapping("/backendinfo")
    public ModelMap tampilkanBackendInfo() {
        log.info("Menampilkan backend info");
        return new ModelMap()
                .addAttribute("info",
                        pembayaranClientService.backendInfo());
    }
}
