package com.muhardin.endy.training.microservices.frontend.service;

import com.muhardin.endy.training.microservices.frontend.dto.Customer;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

public interface PembayaranClientService {
    @GetMapping("/api/customer/")
    Iterable<Customer> daftarCustomer();
    //Iterable<Customer> daftarCustomer(@RequestHeader("Authorization") String authHeader);

    @GetMapping("/api/backendinfo")
    Map<String, Object> backendInfo();
}
